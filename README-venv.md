# Python virtual environment
The file contains Bash commands that can be handy while working with Python 3 and virtual environments in Ubuntu.

Create a virtual environment in **venv** folder
```
python3 -m venv venv
```

Activate the virtual environment
```
source venv/bin/activate
```

Verify that the correct Python is being used
```
which python3
```

Install Python packages from **requirements.txt**
```
python3 -m pip install -r requirements.txt
```

Save a list of installed Python packages to **requirements.txt**
```
python3 -m pip freeze > requirements.txt
```

Deactivate the virtual environment
```
deactivate
```

Remove the created virtual environment
```
rm -rf venv
```
