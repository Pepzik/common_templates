# Ubuntu in WSL
The file contains Powershell commands that can be handy while working with WSL. Ubuntu distributions can be found on the page https://cloud-images.ubuntu.com/releases/.

Download Ubuntu 20.04 distro with Powershell
```
Invoke-WebRequest -Uri "http://cloud-images-archive.ubuntu.com/releases/focal/release-20200423/ubuntu-20.04-server-cloudimg-amd64-wsl.rootfs.tar.gz" -OutFile ubuntu-20.04-server-cloudimg-amd64-wsl.rootfs.tar.gz
```

Import distro with name **wsl-ubuntu** to folder **wsl-ubuntu-folder**
```
wsl --import wsl-ubuntu wsl-ubuntu-folder ubuntu-20.04-server-cloudimg-amd64-wsl.rootfs.tar.gz
```

Run and connect to the imported distro
```
wsl -d wsl-ubuntu
```

Exit the distro
```
exit
```

Show all WSL distributions
```
wsl --list
```

Show running WSL distributions
```
wsl --list --running
```

Shutdown the distro
```
wsl --terminate wsl-ubuntu
```

Export the distro to the template
```
wsl --export wsl-ubuntu wsl-ubuntu-template.tar.gz
```

Unregister the distro
```
wsl --unregister wsl-ubuntu
```

Remove the distro folder
```
rm -R -Force wsl-ubuntu-folder
```
